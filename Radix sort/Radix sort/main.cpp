//Laboratory work 8 "Sorting methods: radix sorting..."
//A sequence of numbers is given. Sort and output a sequence of numbers
// by a certain method.

#include <iostream>
#include <cmath>
#include <chrono>

using std::cin;
using std::cout;

const int N = 1000;

class Timer
{
private:
	using clock_t = std::chrono::steady_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

void RadixSortLSD(int* mas, int n) {
	int bucket[N]= {}, maxVal = 0, digitPosition = 1;
	for (int i = 0; i < n; i++) {
		if (mas[i] > maxVal)
			maxVal = mas[i];
	}

	//maxVal defines the number of cycles
	while (maxVal / digitPosition > 0) {
		//reset counter 
		int digitCount[10] = { 0 };
		//count digits (keys)
		for (int i = 0; i < n; ++i) {
			digitCount[mas[i] / digitPosition % 10]++;
		}

		for (int i = 1; i < 10; ++i) {
			digitCount[i] += digitCount[i - 1];
		}
		
		for (int i = n - 1; i >= 0; --i) {
			bucket[--digitCount[mas[i] / digitPosition % 10]] = mas[i];
		}
		//rearrange the original array using elements in the bucket 
		for (int i = 0; i < n; i++) {
			mas[i] = bucket[i];
		}
		//move up the digit position
		digitPosition *= 10;
	}
}

int main() {
	//std::setlocale(LC_ALL, "Russian");
	int* ptr = new int[N];
	int n;
	cin >> n;
	Timer t;
	for (int i = 0; i < n; ++i) {
		ptr[i] = rand() % 100 + rand() % 10;
	}
	cout << "Source array: ";
	for (int i = 0; i < n; ++i) {
		cout << ptr[i] << " ";
	}
	RadixSortLSD(ptr, n);
	std::cout << "\nRadix sort's time elapsed: " << t.elapsed() << " seconds\n";
	cout << "Sorted array: ";
	for (int i = 0; i < n; ++i) {
		cout << ptr[i] << " ";
	}
	delete[] ptr;
}