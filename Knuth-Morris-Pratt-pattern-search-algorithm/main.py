# The laboratory work №12
# Реализовать алгоритм Кнута-Морриса-Пратта для поиска по образцу
'''
Шаги алгоритма:
1) сформировать массив pi (для сдвига по префиксам);
    Префикс-функция - массив чисел, вычисляющийся как наибольшая длина суффикса, совпадающая с её префиксом.
2) поиск образца в строке.
'''
def PrefixFunc (s):
    l = len(s)
    pi = [0]*l
    j, i = 0, 1
    while i < l:
        if s[i] != s[j]:
            if j == 0:
                pi[i] = 0
                i += 1
            else:
                j = pi[j-1]
        else:
            pi [i] = j + 1
            i += 1
            j += 1
    return pi

def KMP (txt, s):
    s_len = len(s)
    txt_len = len(txt)
    print('Длина текста: ',txt_len,'\n','Длина образца: ', s_len)

    if not txt_len or s_len > txt_len:
        return []

    pi = PrefixFunc(s)
    print(pi)

    entries = []

    i = j = 0

    while i < txt_len and j < s_len:
        if txt[i] == s[j]:
            if j == s_len - 1:
                entries.append(i - s_len + 1)
                j = 0
            else:
                j += 1
            i += 1
        elif j > 0:     # j > 0
            j = pi[j-1]
        else:
            i += 1
    if entries == []:
        print('Образец не найден!')
    else:
        print('Образец найден!')

    return entries


with open('input.txt', 'r') as f:
    txt = str(f.readline())

s = input('Образец: ')

pi = KMP(txt, s)
print(pi)
