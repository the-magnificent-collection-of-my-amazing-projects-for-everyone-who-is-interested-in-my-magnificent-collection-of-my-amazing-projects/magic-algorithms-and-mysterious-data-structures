#include <iostream>
#include "functions.hpp"

bool Expression(S_t& Stack_n, S_t& Stack_o, Leksema item) {
    double a, b, c;
    a = Stack_n.top().value; // ���������� ������� ������� �����
    Stack_n.pop();
    switch (Stack_o.top().type) {
    case '+':
        b = Stack_n.top().value;
        Stack_n.pop();
        c = a + b;
        item.type = '0';
        item.value = c;
        Stack_n.push(item);
        Stack_o.pop();
        break;

    case '-':
        b = Stack_n.top().value;
        Stack_n.pop();
        c = b - a;
        item.type = '0';
        item.value = c;
        Stack_n.push(item);
        Stack_o.pop();
        break;

    case '*':
        b = Stack_n.top().value;
        Stack_n.pop();
        c = b * a;
        item.type = '0';
        item.value = c;
        Stack_n.push(item);
        Stack_o.pop();
        break;

    case '/':
        b = Stack_n.top().value;
        if (a == 0) {
            std::cerr << "������� �� ���� �����������!" << '\n';
        }
        else {
            Stack_n.pop();
            c = b / a;
            item.type = '0';
            item.value = c;
            Stack_n.push(item);
            Stack_o.pop();
            break;
        }
    default:
        std::cerr << "������" << '\n';
        return false;
        break;
    }
    return true;
}

int Get(char ch) {
    if (ch == '+' || ch == '-') return 1;
    if (ch == '/' || ch == '*') return 2;
    return 0;
}
