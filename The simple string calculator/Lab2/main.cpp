﻿//Лаба №2 "Задача об арифметическом выражении"
//На вход подаётся математическое выражение.Элементы - числа.Операции - "+ - * /".
// Также есть скобочки.Окончанием выражения служит "=".Программа должна вывести результат выражения
//
//Пример ввода :
//2 + 7 * (3 / 9) - 5 =
//
//Замечание :
//Программа также должна делать "проверку на дурака" : нет деления на 0, все скобки стоят верно(см лабу №1) и т.п.

#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include "functions.hpp"

using std::cin;
using std::cout;

int main() {
    setlocale(LC_ALL, "Rus");
    char ch=0;
    double value;
    S_t Stack_n;
    S_t Stack_o;
    
    Leksema item = { '0',1 };

    while (1) {
        ch = cin.peek(); // считывает следующий символ из входного потока, не извлекая его
        //cout << ch << '\n';
        if (ch == '=') break;
        if (ch == ' ') {
            cin.ignore();
            continue;
        }
        if (ch >= '0' && ch <= '9') {
            cin >> value;
            item.type = '0';
            item.value = value;
            Stack_n.push(item);
            continue;
        }
        if (ch == '-' || ch == '+' || ch == '*' || ch == '/') {
            if (Stack_o.size() == 0) { // если стек с операциями пуст
                item.type = ch;
                item.value = 0;
                Stack_o.push(item);
                cin.ignore(); // игнорирование символа
                continue;
            }
            if (Stack_o.size() != 0 && Get(ch) > Get(Stack_o.top().type)) { // приоритет операции выше верхней в стеке
                item.type = ch;
                item.value = 0;
                Stack_o.push(item);
                cin.ignore(); // игнорирование символа
                continue;
            }
            if (Stack_o.size() != 0 && Get(ch) <= Get(Stack_o.top().type)) { // приоритет операции <= верхней в стеке
                if (Expression(Stack_n, Stack_o, item) == 0) {
                    system("pause");
                    return 0;
                }
                continue;
            }
        }
        if (ch == '(' || ch == '[' || ch == '{') {
            item.type = ch;
            item.value = 0;
            Stack_o.push(item);
            cin.ignore(); // игнорирование символа
            continue;
        }
        if (ch == ')') {
            if (Stack_o.size() == 0) {
                cout << "Строка не существует" << '\n';
                system("pause");
            }
            /*if (Stack_o.top().type != '(') {
                    cout << "Строка не существует" << '\n';
                    system("pause");
                    return 0;
                }*/
            else {
                while (Stack_o.top().type != '(') {
                    if (Stack_o.top().type == '{' || Stack_o.top().type == '[') {
                        cout << "Строка не существует" << '\n';
                        system("pause");
                        return 0;
                    }
                    if (Expression(Stack_n, Stack_o, item) == 0) {
                        system("pause");
                        return 0;
                    }
                    else continue;
                }
            }
            Stack_o.pop();
            cin.ignore();
            continue;
        }
        if (ch == '}') {
            if (Stack_o.size() == 0) {
                cout << "Строка не существует" << '\n';
                system("pause");
            }
            /*if (Stack_o.top().type != '{') {
                cout << "Строка не существует" << '\n';
                system("pause");
                return 0;
            }*/
            else {
                while (Stack_o.top().type != '{') {
                    if (Stack_o.top().type == '(' || Stack_o.top().type == '[') {
                        cout << "Строка не существует" << '\n';
                        system("pause");
                        return 0;
                    }
                    if (Expression(Stack_n, Stack_o, item) == 0) {
                        system("pause");
                        return 0;
                    }
                    else continue;
                }
            }
            Stack_o.pop();
            cin.ignore();
            continue;
        }
        if (ch == ']') {
            if (Stack_o.size() == 0) {
                cout << "Строка не существует" << '\n';
                system("pause");
            }
            /*if (Stack_o.top().type != '(') {
                cout << "Строка не существует" << '\n';
                system("pause");
                return 0;
            }*/
            else {
                while (Stack_o.top().type != '[') {
                    if (Stack_o.top().type == '{' || Stack_o.top().type == '(') {
                        cout << "Строка не существует" << '\n';
                        system("pause");
                        return 0;
                    }
                    if (Expression(Stack_n, Stack_o, item) == 0) {
                        system("pause");
                        return 0;
                    }
                    else continue;
                }
            }
            Stack_o.pop();
            cin.ignore();
            continue;
        }
        else {
            std::cerr << "Вы ввели странный символ" << "\n";
        }
    }
    while (Stack_o.size() != 0) {
        if (Expression(Stack_n, Stack_o, item) == 0) {
            system("pause");
            return 0;
        }
        else continue;
    }
    
        cout << "Result: " << Stack_n.top().value << '\n';
        return 0;
    
}


