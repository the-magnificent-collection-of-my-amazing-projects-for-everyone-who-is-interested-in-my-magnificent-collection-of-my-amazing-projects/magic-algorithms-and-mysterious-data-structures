// ���4 "������ ����������: ���������� ������� ������������..."
// ���� ������������������ �����. ������������� � ������� ������������������ �����
// ����������� �������.

#include <iostream>
#include <cmath>
#include <chrono>

using std::cin;
using std::cout;

const int N = 1000;

class Timer
{
private:
	using clock_t = std::chrono::steady_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

// Function to sort mas[0..n-1], using Comb Sort
void CombSort(int* mas, int n) {
	//Initialize gap
	int gap = n;
	double shrink = 1.3;

	// Initialized sorted as true to make sure that loop runs
	bool sorted = true;

	while (sorted == true) {
		// To find gap between elements
		// Shrink gap by Shrink factor
		gap = floor(gap / shrink);
		// floor() computes the largest integer value not greater than arg

		if (gap <= 1) {
			gap = 1;
			sorted = false; // break
		}

		// Keep running while gap is more than 1 and last
		// iteration caused a swap
		for (int i = 0; i < n - gap; ++i) {
			if (mas[i] > mas[i + gap]) {
				std::swap(mas[i], mas[i + gap]);
				sorted = true;
			}
		}
	}
}

int main() {
	int* ptr = new int[N];
	int n;
	cin >> n;
	Timer t;
	for (int i = 0; i < n; ++i) {
		ptr[i] = rand() % 100 + rand() % 10;
	}
	cout << "Source array: ";
	for (int i = 0; i < n; ++i) {
		cout << ptr[i] << " ";
	}
	CombSort(ptr, n);
	std::cout << "\nComb sort's time elapsed: " << t.elapsed() << " seconds\n";
	cout << "Sorted array: ";
	for (int i = 0; i < n; ++i) {
		cout << ptr[i] << " ";
	}
	delete[] ptr;
}